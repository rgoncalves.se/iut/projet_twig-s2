<?php

class AdherentController
{
	private $instanceModelEmprunt;
	private $instanceModelAdherent;

	public function __construct() {
		include("Model/EmpruntModel.php");
		include("Model/AdherentModel.php");
		$this->instanceModelEmprunt = new EmpruntModel();
		$this->instanceModelAdherent = new AdherentModel();
		$loader = new \Twig\Loader\FilesystemLoader(MAIN_PATH . 'views/');
		$this->twig = new \Twig\Environment($loader, ['debug' => true]);
		$this->twig->addGlobal('BASE_URL', BASE_URL);
		$this->twig->addGlobal('ASSET_URL', ASSET_URL);
	}

	public function index() {
		echo $this->twig->render('layout.html.twig');
	}

	public function afficherAdherents()
	{
		$adherents = $this->instanceModelAdherent->findAllAdherents();
		echo $this->twig->render('adherent/showAdherents.html.twig', ['adherents' => $adherents]);
	}

	public function creerAdherent()
	{
		$aujourdhui = new DateTime();
		$donnees['datePaiement'] = $aujourdhui->format('d/m/Y');
		echo $this->twig->render('adherent/addAdherent.html.twig', ['adherent' => $donnees]);
	}

	public function validFormCreerAdherent() {
		$donnees['nomAdherent'] = $_POST['nomAdherent'];
		$donnees['adresse'] = htmlentities($_POST['adresse']);
		$donnees['datePaiement'] = htmlentities($_POST['datePaiement']);
		$erreurs = $this->validatorAdherent($donnees);
		if(empty($erreurs)) {
			$donnees['nomAdherent'] = addslashes($donnees['nomAdherent']);
			$donnees['datePaiement_us'] = Datetime::createFromFormat('d/m/Y', $donnees['datePaiement'])->format('Y-m-d');
			$this->instanceModelAdherent->createAndPersistAdherent($donnees);
			header("Location: ".BASE_URL."/adherent/afficherAdherents");
		}
		echo $this->twig->render('adherent/addAdherent.html.twig', ['erreurs' => $erreurs, 'donnees' => $donnees]);
	}

	public function supprimerAdherent($id = '') {
		$donnees = $this->instanceModelEmprunt->findByIdAdherentEmprunt($id);
		$nombre = count($donnees);
		if(empty($donnees)) {
			$this->instanceModelAdherent->removeByIdAdherent($id);
			header("Location: ".BASE_URL."/adherent/afficherAdherents");
		}
		echo $this->twig->render('adherent/ErrorDeleteAdherent.html.twig', ['nombre' => $nombre]);
	}

	public function modifierAdherent($id = '') {
		$adherent = $this->instanceModelAdherent->findOneByIdAdherent($id);
		$adherent['datePaiement'] = DateTime::createFromFormat('Y-m-d', $adherent['datePaiement'])->format('d/m/Y');
		echo $this->twig->render('adherent/editAdherent.html.twig', ['donnees' => $adherent]);
	}

	public function validFormModifierAdherent() {
		$aujourdhui = new DateTime();
		$donnees['idAdherent'] = htmlentities($_POST['idAdherent']);
		$donnees['nomAdherent'] = addslashes($_POST['nomAdherent']);
		$donnees['adresse'] = htmlentities($_POST['adresse']);
		$donnees['datePaiement'] = htmlentities($_POST['datePaiement']);

		$erreurs = $this->validatorAdherent(($donnees));
		if(empty($erreurs)) {
			$date = DateTime::createFromFormat('d/m/Y', $donnees['datePaiement']);
			$donnees['datePaiement'] = $date->format('Y-m-d');
			$this->instanceModelAdherent->updateAndPersistAdherent($donnees['idAdherent'], $donnees);
			header("Location: ".BASE_URL."/adherent/afficherAdherents");
		}
		echo $this->twig->render('adherent/editAdherent.html.twig', ['erreurs' => $erreurs, 'donnees' => $donnees]);
	}

	public function validatorAdherent($donnees){
		$erreurs = array();
		if(!preg_match("/^[A-Za-z ]{1,}/",$donnees['nomAdherent']))
			$erreurs['nomAdherent'] = 'Le nom doit être composé de 2 lettres minimum.';
		if(!preg_match("/^[A-Za-z ]{1,}/",$donnees['adresse']))
			$erreurs['adresse'] = 'L\'adresse doit être composée de 2 lettres minimum.';
		$dateConvert = DateTime::createFromFormat('d/m/Y', $donnees['datePaiement']);
		if($dateConvert == NULL)
			$erreurs['datePaiement'] = 'La date doit être au format JJ/MM/AAAA.';
		else {
			if($dateConvert->format('d/m/Y') != $donnees['datePaiement'])
				$erreurs['datePaiement'] = 'La date n\'est pas valide (format JJ/MM/AAAA).';
		}
		return $erreurs;
	}

}



