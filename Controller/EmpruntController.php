<?php
class EmpruntController
{
    private $instanceModelAdherent;
    private $instanceModelExemplaire;
    private $instanceModelEmprunt;


    public function __construct(){
        include("Model/AdherentModel.php");
        include("Model/EmpruntModel.php");
        include("Model/ExemplaireModel.php");
        $this->instanceModelAdherent = new AdherentModel();
        $this->instanceModelExemplaire = new ExemplaireModel();
        $this->instanceModelEmprunt = new EmpruntModel();
        $loader = new \Twig\Loader\FilesystemLoader(MAIN_PATH.'views/');
        $this->twig = new \Twig\Environment($loader, ['debug' => true]);
        $this->twig->addGlobal('BASE_URL', BASE_URL);
        $this->twig->addGlobal('ASSET_URL', ASSET_URL);
    }

    public function index()
    {
        echo $this->twig->render('layout.html.twig');
    }

    // addEmpunts

    public function selectAdherentEmprunts()
    {
        echo $this->twig->render('layout.html.twig',['error' => 'selectAdherentEmprunts : action manquante']);
    }

    public function addEmpruntsShowEmpruntAdherent()
    {
    }

    public function addEmprunts()
    {

        
    }

    // deleteAllEmpunts
    public function deleteAllEmprunts()
    {
     
    }

    public function validFormAddDeleteAllEmprunts($id='')
    {

  
    }

    //returnEmprunts

    public function returnEmprunts($id='')
    {
   
    }

    public function bilanEmprunts()
    {

    }


    public function validatorEmprunt($donnees)
    {
        $erreurs=array();
      
        return $erreurs;
    }

    public function validatorRetour($donnees)
    {
        $erreurs=array();
    
        return $erreurs;
    }

}




