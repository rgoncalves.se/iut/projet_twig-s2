<?php
class ExemplaireController
{
    private $instanceModelOeuvre;
    private $instanceModelExemplaire;
    private $instanceModelAuteur;


    public function __construct(){
        include("Model/OeuvreModel.php");
        include("Model/EmpruntModel.php");
        include("Model/ExemplaireModel.php");
        $this->instanceModelOeuvre = new OeuvreModel();
        $this->instanceModelExemplaire = new ExemplaireModel();
        $this->instanceModelEmprunt = new EmpruntModel();
        $loader = new \Twig\Loader\FilesystemLoader(MAIN_PATH.'views/');
        $this->twig = new \Twig\Environment($loader, ['debug' => true]);
        $this->twig->addGlobal('BASE_URL', BASE_URL);
        $this->twig->addGlobal('ASSET_URL', ASSET_URL);
    }

    public function index()
    {
        echo $this->twig->render('layout.html.twig');
    }
    public function afficherExemplaires($noOeuvre)
    {
        $donnees=$this->instanceModelExemplaire->findAllExemplairesByOeuvre($noOeuvre);
        $donnees2=$this->instanceModelExemplaire->findDetailsAllExemplairesByOeuvre($noOeuvre);
        echo $this->twig->render('exemplaire/showExemplaires.html.twig',['donnees' => $donnees, 'donnees2' => $donnees2 ]);
    }
    public function creerExemplaire($noOeuvre)
    {
        $donnees['etat']='NEUF';
        $donnees['dateAchat']=date('d/m/Y');
        $donnees['noOeuvre'] = htmlentities($noOeuvre);
        $donnees2=$this->instanceModelExemplaire->findDetailsAllExemplairesByOeuvre($noOeuvre);
      //  dump($donnees);
        echo $this->twig->render('exemplaire/addExemplaire.html.twig',['donnees' => $donnees, 'donnees2' => $donnees2 ]);
    }

    public function validFormCreerExemplaire()
    {
        $donnees['etat'] = $_POST['etat'];
        $donnees['dateAchat'] = htmlentities($_POST['dateAchat']);
        $donnees['prix'] = htmlentities($_POST['prix']);
        $donnees['noOeuvre'] = htmlentities($_POST['noOeuvre']);

        $erreurs=$this->validatorExemplaire($donnees);
        if(empty($erreurs)) {
            $donnees['dateAchat_us'] = DateTime::createFromFormat('d/m/Y', $donnees['dateAchat'])->format('Y-m-d');
            $this->instanceModelExemplaire->createAndPersistExemplaire($donnees);
            $URI='/Exemplaire/afficherExemplaires/'.$donnees['noOeuvre'];
            header("Location: ".BASE_URL.$URI);
        }
        $donnees2=$this->instanceModelExemplaire->findDetailsAllExemplairesByOeuvre($donnees['noOeuvre']);
        echo $this->twig->render('exemplaire/addExemplaire.html.twig',['donnees' => $donnees, 'donnees2' => $donnees2 , 'erreurs' => $erreurs]);
    }

    public function supprimerExemplaire($noExemplaire='')
    {
        $dataEmpruntExemplaire=$this->instanceModelEmprunt->findByIdExemplaireEmprunt($noExemplaire);
        $dataExemplaire=$this->instanceModelExemplaire->findOneById($noExemplaire);

        if(empty($dataEmpruntExemplaire)){
            $this->instanceModelExemplaire->removeByIdExemplaire($noExemplaire);
            $URI='/Exemplaire/afficherExemplaires/'.$dataExemplaire['noOeuvre'];
            header("Location: ".BASE_URL.$URI);
        }
        echo $this->twig->render('exemplaire/ErrorDeleteExemplaire.html.twig',['empruntsExemplaire' => $dataEmpruntExemplaire, 'exemplaire' => $dataExemplaire]);
    }



    public function modifierExemplaire($noExemplaire='')
    {
        $donnees=$this->instanceModelExemplaire->findOneById($noExemplaire);
        $donnees2=$this->instanceModelExemplaire->findDetailsAllExemplairesByOeuvre($donnees['noOeuvre']);
        if($donnees['dateAchat']){
            list($year,$month, $day )  = explode('-', $donnees['dateAchat']);
            $donnees['dateAchat']=$day."/".$month."/".$year;
        }
        echo $this->twig->render('exemplaire/editExemplaire.html.twig',['donnees' => $donnees, 'donnees2' => $donnees2 ]);
    }

    public function validFormModifierExemplaire()
    {
        $donnees['noOeuvre']=$_POST['noOeuvre'];
        // ## contrôles des données
        $donnees['noExemplaire']=$_POST['noExemplaire'];
        $donnees['etat']=htmlentities($_POST['etat']);
        $donnees['dateAchat']=htmlentities($_POST['dateAchat']);
        $donnees['prix']=htmlentities($_POST['prix']);


        $erreurs=$this->validatorExemplaire($donnees);
        if(empty($erreurs)) {
            $donnees['dateAchat_us'] = DateTime::createFromFormat('m/d/Y', $donnees['dateAchat'])->format('Y-m-d');
            $this->instanceModelExemplaire->updateAndPersistExemplaire($donnees['noExemplaire'], $donnees);
            $URI='/Exemplaire/afficherExemplaires/'.$donnees['noOeuvre'];
            header("Location: ".BASE_URL.$URI);
        }
        $donnees2=$this->instanceModelExemplaire->findDetailsAllExemplairesByOeuvre($donnees['noOeuvre']);
        dump($donnees);       dump($donnees2);dump($erreurs);
        echo $this->twig->render('exemplaire/editExemplaire.html.twig',['donnees' => $donnees, 'donnees2' => $donnees2 , 'erreurs' => $erreurs]);

    }


    public function validatorExemplaire($donnees)
    {
        $erreurs=array();
        if (!preg_match("/^[0-9.]{1,}$/", $donnees['prix'])) $erreurs['prix'] = 'saisir un prix au format dd.dd';

        $dateConvert=DateTime::createFromFormat('d/m/Y',$donnees['dateAchat']);
        if($dateConvert==NULL)
            $erreurs['dateAchat']='la date doit être au format JJ/MM/AAAA';
        else{
            if($dateConvert->format('d/m/Y') !== $donnees['dateAchat']){
                $erreurs['dateAchat']='la date n\'est pas valide (format JJ/MM/AAAA)';
              //  var_dump($dateConvert->format('d/m/Y'));var_dump($donnees['dateAchat']);
            }

        }
        return $erreurs;
    }

}





