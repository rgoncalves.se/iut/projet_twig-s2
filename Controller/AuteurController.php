<?php

class AuteurController
{
	private $instanceModelOeuvre;
	private $instanceModelAuteur;

	public function __construct()
	{
		include("Model/OeuvreModel.php");
		include("Model/AuteurModel.php");
		include("Model/ExemplaireModel.php");
		$this->instanceModelOeuvre = new OeuvreModel();
		$this->instanceModelAuteur = new AuteurModel();
		$loader = new \Twig\Loader\FilesystemLoader(MAIN_PATH . 'views/');
		$this->twig = new \Twig\Environment($loader, ['debug' => true]);
		$this->twig->addGlobal('BASE_URL', BASE_URL);
		$this->twig->addGlobal('ASSET_URL', ASSET_URL);
	}

	public function index()
	{
		echo $this->twig->render('layout.html.twig');
	}

	public function afficherAuteurs()
	{
		$auteurs = $this->instanceModelAuteur->findAllAuteurs();
		echo $this->twig->render('auteur/showAuteurs.html.twig', ['auteurs' => $auteurs]);
	}

	public function creerAuteur()
	{
		echo $this->twig->render('auteur/addAuteur.html.twig');
	}

	public function validFormCreerAuteur()
	{
		$donnees['nomAuteur'] = $_POST['nomAuteur'];
		$donnees['prenomAuteur'] = htmlentities($_POST['prenomAuteur']);
		$erreurs = $this->validatorAuteur($donnees);
		if(empty($erreurs)) {
			$donnes['nomAuteur'] = addslashes($donnees['nomAuteur']);
			$this->instanceModelAuteur->createAndPersistAuteur($donnees);
			header("Location: ".BASE_URL."/Auteur/afficherAuteurs");
		}
		echo $this->twig->render('auteur/addAuteur.html.twig', ['erreurs' => $erreurs, 'donnees' => $donnees]);
	}

	public function supprimerAuteur($id = '')
	{
		$donnees = $this->instanceModelOeuvre->findByIdAuteurOeuvre($id);
		$nombre = count($donnees);
		if(empty($donnees)) {
			$this->instanceModelAuteur->removeByIdAuteur($id);
			header("Location: ".BASE_URL."/Auteur/afficherAuteurs");
		}
		echo $this->twig->render('auteur/ErrorDeleteAuteur.html.twig', ['nombre' => $nombre]);
	}

	public function modifierAuteur($id = '')
	{
		$auteur = $this->instanceModelAuteur->findOneByIdAuteur($id);
		echo $this->twig->render('auteur/editAuteur.html.twig', ['donnees' => $auteur]);
	}

	public function validFormModifierAuteur()
	{
		$donnees['nomAuteur'] = addslashes($_POST['nomAuteur']);
		$donnees['prenomAuteur'] = htmlentities($_POST['prenomAuteur']);
		$donnees['idAuteur'] = htmlentities($_POST['idAuteur']);

		$erreurs = $this->validatorAuteur(($donnees));
		if(empty($erreurs)) {
			$this->instanceModelAuteur->updateAndPersistAuteur($donnees['idAuteur'], $donnees);
			header("Location: ".BASE_URL."/auteur/afficherAuteurs");
		}
		echo $this->twig->render('auteur/editAuteur.html.twig', ['erreurs' => $erreurs, 'donnees' => $donnees]);
	}


	public function validatorAuteur($donnees)
	{
		$erreurs = array();
		if (!preg_match("/^[A-Za-z ]{2,}/", $donnees['nomAuteur']))
			$erreurs['nomAuteur'] = 'nom composé de 2 lettres minimum';
		return $erreurs;
	}

}



