<?php
class OeuvreController
{
    private $instanceModelOeuvre;
    private $instanceModelExemplaire;
    private $instanceModelAuteur;

    public function __construct(){
        include("Model/OeuvreModel.php");
        include("Model/AuteurModel.php");
        include("Model/ExemplaireModel.php");
        $this->instanceModelOeuvre = new OeuvreModel();
        $this->instanceModelExemplaire = new ExemplaireModel();
        $this->instanceModelAuteur = new AuteurModel();
        $loader = new \Twig\Loader\FilesystemLoader(MAIN_PATH.'views/');
        $this->twig = new \Twig\Environment($loader, ['debug' => true]);
        $this->twig->addGlobal('BASE_URL', BASE_URL);
        $this->twig->addGlobal('ASSET_URL', ASSET_URL);
    }

    public function index()
    {
        echo $this->twig->render('layout.html.twig');
    }
    public function afficherOeuvres()
    {
        $oeuvres = $this->instanceModelOeuvre->findAllOeuvres();
        echo $this->twig->render('oeuvre/showOeuvre.html.twig', ['oeuvres' => $oeuvres]);
    }
    public function creerOeuvre()
    {
        $donnees['dateParution'] = (new DateTime())->format("d/m/Y");
        $auteurs = $this->instanceModelAuteur->findAllAuteurs();
        echo $this->twig->render('oeuvre/addOeuvre.html.twig', ['auteurs' => $auteurs, 'donnees' => $donnees]);
    }

    public function validFormCreerOeuvre()
    {
        $donnees['titre'] = $_POST['titre'];
        $donnees['dateParution'] = htmlentities($_POST['dateParution']);
        $donnees['idAuteur'] = htmlentities($_POST['idAuteur']);
        $donnees['photo'] = htmlentities($_POST['photo']);
        $erreurs = $this->validatorOeuvre($donnees);
        if(empty($erreurs)) {
            $donnees['dateParution_us'] = Datetime::createFromFormat('d/m/Y', $donnees['dateParution'])->format('Y-m-d');
            $this->instanceModelOeuvre->createAndPersistOeuvre($donnees);
            header("Location: ".BASE_URL."/oeuvre/afficherOeuvres");
        }
        $auteurs = $this->instanceModelAuteur->findAllAuteurs();
        echo $this->twig->render('oeuvre/addOeuvre.html.twig', ['erreurs' => $erreurs, 'donnees' => $donnees, 'auteurs' => $auteurs]);
    }

    public function supprimerOeuvre($id='')
    {
        $donnees = $this->instanceModelExemplaire->findByIdOeuvreExemplaire($id);
        $nombre = count($donnees);
        if(empty($donnees)) {
            $this->instanceModelOeuvre->removeByIdOEuvre($id);
            header("Location: ".BASE_URL."/oeuvre/afficherOeuvres");
        }
        echo $this->twig->render('oeuvre/ErrorDeleteOeuvre.html.twig', ['nombre' => $nombre]);
    }



    public function modifierOeuvre($id='')
    {
        $donnees = $this->instanceModelOeuvre->findOneByIdOeuvre($id);
        $donnees['dateParution'] = DateTime::createFromFormat('Y-m-d', $donnees['dateParution'])->format('d/m/Y');
        $auteurs = $this->instanceModelAuteur->findAllAuteurs();
        echo $this->twig->render('oeuvre/editOeuvre.html.twig', ['donnees' => $donnees, 'auteurs' => $auteurs]);
    }

    public function validFormModifierOeuvre()
    {
        $aujourdhui = new DateTime();
        $donnees['noOeuvre'] = htmlentities($_POST['noOeuvre']);
        $donnees['titre'] = addslashes($_POST['titre']);
        $donnees['dateParution'] = htmlentities($_POST['dateParution']);
        $donnees['photo'] = htmlentities($_POST['photo']);
        $donnees['idAuteur'] = htmlentities($_POST['idAuteur']);

        $erreurs = $this->validatorOeuvre(($donnees));
        if(empty($erreurs)) {
            $date = DateTime::createFromFormat('d/m/Y', $donnees['dateParution']);
            $donnees['datePaiement'] = $date->format('Y-m-d');
            $this->instanceModelOeuvre->updateAndPersistOeuvre($donnees['noOeuvre'], $donnees);
            header("Location: ".BASE_URL."/oeuvre/afficherOeuvres");
        }
        echo $this->twig->render('oeuvre/editOeuvre.html.twig', ['erreurs' => $erreurs, 'donnees' => $donnees]);
    }


    public function validatorOeuvre($donnees)
    {
        $erreurs = array();
        if(!preg_match("/^[A-Za-z ]{2,}/", $donnees['titre'])) {
            $erreurs['titre'] = 'Nom composé de 2 lettres minimum.';
        }
        if(!is_numeric($donnees['idAuteur'])) {
            $erreurs['idAuteur'] = 'Saisir une valeur.';
        }
        $dateConvert = DateTime::createFromFormat('d/m/Y', $donnees['dateParution']);
        if($dateConvert == NULL) {
            $erreurs['dateParution'] = 'La date doit être au format JJ/MM/AAAA.';
        } else {
            if($dateConvert->format('d/m/Y') != $donnees['dateParution']) {
                $erreurs['dateParution'] = 'La date n\'est pas valide (format JJ/MM/AAAA).';
            }
        }
        if($donnees['photo'] != "") {
            $file = './assets/images/'.$donnees['photo'];
            if(!file_exists($file)) {
                $erreurs['photo'] = 'La photo n\'existe pas dans le dossiers assets/images/';
            }
        }
        return $erreurs;
    }

}


