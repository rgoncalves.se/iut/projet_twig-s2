<?php
class IndexController
{
    public function index()
    {
        $loader = new \Twig\Loader\FilesystemLoader(MAIN_PATH.'views/');
        $this->twig = new \Twig\Environment($loader, ['debug' => true]);
        $this->twig->addGlobal('BASE_URL', BASE_URL);
        $this->twig->addGlobal('ASSET_URL', ASSET_URL);
        echo $this->twig->render('layout.html.twig');
    }
}

