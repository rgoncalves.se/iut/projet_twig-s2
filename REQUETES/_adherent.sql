/* TP4
 * Script ADHERENT
 */

-- Requête 2a
-- afficher infos adhérents + date limite renouvellement cotisation + nombre d'emprunts en cours

SELECT adherent.nomAdherent
	, adherent.adresse
	, adherent.dateAchat
	, adherent.idAdherent
	, COUNT(emprunt.idAdherent) AS nbrEmprunt
	, DATE_ADD(adherent.dateAchat, INTERVAL 1 YEAR) AS datePaiementFutur
	FROM adherent
	LEFT JOIN emprunt
		ON emprunt.idAdherent = adherent.idAdherent
		AND emprunt.dateRendu = '0000-00-00'
	GROUP BY adherent.idAdherent
	ORDER BY adherent.nomAdherent;

-- Requête 2b
-- afficher infos adhérents + date limite renouvellement cotisation + nombre d'emprunts en cours
-- ajout de flags en cas de retard + retartd proche

SELECT adherent.nomAdherent
	, adherent.adresse
	, adherent.dateAchat
	, adherent.idAdherent
	, COUNT(emprunt.idAdherent) AS nbrEmprunt
	, IF(
		CURRENT_DATE() > DATE_ADD(adherent.dateAchat, INTERVAL 1 YEAR),1,0) AS retard
	, IF(
		CURRENT_DATE() > DATE_ADD(adherent.dateAchat, INTERVAL 11 MONTH),1,0) AS retardProche
	, DATE_ADD(adherent.dateAchat, INTERVAL 1 YEAR) AS datePaiementFutur
	FROM adherent
	LEFT JOIN emprunt
		ON emprunt.idAdherent = adherent.idAdherent
		AND emprunt.dateRendu = '0000-00-00'
	GROUP BY adherent.idAdherent
	ORDER BY adherent.nomAdherent;
