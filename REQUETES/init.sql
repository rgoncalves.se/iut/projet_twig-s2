#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------

#------------------------------------------------------------
# Drop Table
#------------------------------------------------------------

DROP TABLE IF EXISTS emprunt;
DROP TABLE IF EXISTS adherent;
DROP TABLE IF EXISTS date_emprunt;
DROP TABLE IF EXISTS exemplaire;
DROP TABLE IF EXISTS oeuvre;
DROP TABLE IF EXISTS auteur;

#------------------------------------------------------------
# Table: auteur
#------------------------------------------------------------

CREATE TABLE auteur(
	idAuteur INT Auto_increment NOT NULL
	, nomAuteur VARCHAR(255)
	, prenomAuteur VARCHAR(255)
	, CONSTRAINT auteur_PK PRIMARY KEY (idAuteur)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;


#------------------------------------------------------------
# Table: oeuvre
#------------------------------------------------------------

CREATE TABLE oeuvre(
	noOeuvre INT  Auto_increment  NOT NULL
	, titre VARCHAR(255)
	, dateParution DATE
	, photo VARCHAR(255) NOT NULL
	, idAuteur INT
	, CONSTRAINT oeuvre_PK PRIMARY KEY (noOeuvre)
	, CONSTRAINT oeuvre_auteur_FK FOREIGN KEY (idAuteur) REFERENCES auteur(idAuteur)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;


#------------------------------------------------------------
# Table: exemplaire
#------------------------------------------------------------

CREATE TABLE exemplaire(
	noExemplaire INT Auto_increment NOT NULL
	, etat VARCHAR(255)
	, dateAchat DATE
	, prix DECIMAL(8,2)
	, noOeuvre INT
	, CONSTRAINT exemplaire_PK PRIMARY KEY (noExemplaire)
	, CONSTRAINT exemplaire_oeuvre_FK FOREIGN KEY (noOeuvre) REFERENCES oeuvre(noOeuvre)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;


#------------------------------------------------------------
# Table: adherent
#------------------------------------------------------------

CREATE TABLE adherent(
	idAdherent INT Auto_increment NOT NULL
	, nomAdherent VARCHAR(255)
	, adresse VARCHAR(255)
	, datePaiement DATE
	, CONSTRAINT adherent_PK PRIMARY KEY (idAdherent)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;


#------------------------------------------------------------
# Table: emprunt
#------------------------------------------------------------

CREATE TABLE emprunt(
	idAdherent INT NOT NULL
	, noExemplaire INT NOT NULL
	, dateEmprunt DATE NOT NULL
	, dateRendu DATE
	, CONSTRAINT emprunt_PK PRIMARY KEY (idAdherent,noExemplaire,dateEmprunt)
	, CONSTRAINT emprunt_adherent_FK FOREIGN KEY (idAdherent) REFERENCES adherent(idAdherent)
	, CONSTRAINT emprunt_exemplaire0_FK FOREIGN KEY (noExemplaire) REFERENCES exemplaire(noExemplaire)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;

LOAD DATA LOCAL INFILE 'AUTEUR.csv' 
	INTO TABLE auteur FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INFILE 'OEUVRE.csv' 
	INTO TABLE oeuvre FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INFILE 'EXEMPLAIRE.csv' 
	INTO TABLE exemplaire FIELDS TERMINATED BY ',';
-- LOAD DATA LOCAL INFILE 'DATE_EMPRUNT.csv' 
-- INTO TABLE date_emprunt FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INFILE 'ADHERENT.csv' 
	INTO TABLE adherent FIELDS TERMINATED BY ',';
LOAD DATA LOCAL INFILE 'EMPRUNT.csv' 
	INTO TABLE emprunt FIELDS TERMINATED BY ',';

