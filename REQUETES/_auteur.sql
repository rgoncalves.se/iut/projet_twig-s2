/* TP4
 * Script AUTEUR
 */

-- Requête 1a
-- Afficher les auteurs + nombres d'oeuvres par auteur
SELECT auteur.nomAuteur
	, auteur.prenomAuteur
	, auteur.idAuteur
	, COUNT(oeuvre.idAuteur)
	FROM auteur
	INNER JOIN oeuvre
		ON oeuvre.idAuteur = auteur.idAuteur
	GROUP BY auteur.idAuteur
	ORDER BY auteur.nomAuteur;

-- Requête 1b
-- Afficher tous les auteurs + nombres d'oeuvres par auteur (ainsi que NULL)
SELECT auteur.nomAuteur
	, auteur.prenomAuteur
	, auteur.idAuteur
	, COUNT(oeuvre.idAuteur)
	FROM auteur
	LEFT JOIN oeuvre
		ON oeuvre.idAuteur = auteur.idAuteur
	GROUP BY auteur.idAuteur
	ORDER BY auteur.nomAuteur;


