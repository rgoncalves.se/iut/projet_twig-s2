<?php

class AuteurModel
{

	private $db;

	public function __construct()
	{
		$MaConnexion = new Connexion();
		$this->db = $MaConnexion->connect();
	}

	function findAllAuteurs()
	{
		$requete = "
		SELECT au.nomAuteur
		, au.prenomAuteur
		, au.idAuteur
		, COUNT(oe.noOeuvre) as nbrOeuvre
		FROM oeuvre oe
		RIGHT JOIN auteur au ON au.idAuteur=oe.idAuteur
		GROUP BY au.nomAuteur, au.prenomAuteur, au.idAuteur
		ORDER BY au.nomAuteur;
		";
		$select = $this->db->query($requete);
		$results = $select->fetchAll();
		return $results;
	}


	function createAndPersistAuteur($donnees)
	{
		$requete = "
		INSERT INTO auteur(idAuteur,nomAuteur,prenomAuteur) VALUES
		(NULL, '${donnees['nomAuteur']}', '${donnees['prenomAuteur']}');
		";
		$nbRes = $this->db->exec($requete);
	}


	function removeByIdAuteur($id)
	{
		$requete = "
		DELETE FROM auteur
		WHERE idAuteur=" . $id . " LIMIT 1;
		";
		$nbRes = $this->db->exec($requete);
		return $nbRes;
	}

	function findOneByIdAuteur($id)
	{
		$requete = "
		SELECT idAuteur, nomAuteur, prenomAuteur
		FROM auteur
		WHERE idAuteur = :id";
		$prep = $this->db->prepare($requete);
		$prep->bindParam(':id', $id, PDO::PARAM_INT);
		$prep->execute();
		$result = $prep->fetch();
		return $result;
	}

	function updateAndPersistAuteur($id, $donnees)
	{
		$requete = "
		UPDATE auteur SET
		nomAuteur = '${donnees['nomAuteur']}', prenomAuteur = '${donnees['prenomAuteur']}'
		WHERE idAuteur = ${donnees['idAuteur']};
		";
		$nbRes = $this->db->exec($requete);
		return $nbRes;
	}


	// liste déroulante add/edit OEUVRE
	function findAllDropdownAuteurs()
	{
		return NULL;
	}
}
