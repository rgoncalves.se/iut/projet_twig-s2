<?php


class OeuvreModel{

    private $db;

    public function __construct(){
        $MaConnexion = new Connexion();
        $this->db = $MaConnexion->connect();
    }

    function findAllOeuvres(){
        $requete = "
		SELECT auteur.nomAuteur
		, oeuvre.titre
		, oeuvre.noOeuvre
		, oeuvre.dateParution
		, oeuvre.photo
		, COUNT(E1.noExemplaire) as nbExemplaire
		, COUNT(E2.noExemplaire) as nombreDispo
		, auteur.idAuteur
		FROM oeuvre
		JOIN auteur 
		    ON auteur.idAuteur = oeuvre.idAuteur
        LEFT JOIN exemplaire E1 ON E1.noOeuvre = oeuvre.noOeuvre
        LEFT JOIN exemplaire E2 ON E2.noExemplaire = E1.noExemplaire
            AND E2.noExemplaire NOT IN (SELECT emprunt.noExemplaire FROM emprunt WHERE emprunt.dateRendu IS NULL)
        GROUP BY oeuvre.noOeuvre
        ORDER BY auteur.nomAuteur ASC
        , oeuvre.titre ASC;
		";
        $select = $this->db->query($requete);
        $results = $select->fetchAll();
        return $results;
    }

    function findByIdAuteurOeuvre($id){
    	$requete = "
    	SELECT * FROM oeuvre
    	WHERE idAuteur = ".$id."
    	;";
    	$select = $this->db->query($requete);
    	$results = $select->fetchAll();
        return $results;
    }

    function createAndPersistOeuvre($donnees){
        $date = DateTime::createFromFormat('d/m/Y', $donnees['dateParution']);
        $donnees['dateParution'] = $date->format('Y-m-d');
        $requete = "
		INSERT INTO oeuvre(noOeuvre, titre, dateParution, photo, idAuteur) VALUES
		(NULL, '${donnees['titre']}', '${donnees['dateParution']}', '${donnees['photo']}', '${donnees['idAuteur']}');
		";
        $nbRes = $this->db->exec($requete);
    }


    function removeByIdOeuvre($id){
        $requete = "
		DELETE FROM oeuvre
		WHERE noOeuvre=" . $id . " LIMIT 1;
		";
        $nbRes = $this->db->exec($requete);
        return $nbRes;
    }

    function findOneByIdOeuvre($id){
        $requete = "
		SELECT noOeuvre, titre, dateParution, photo, idAuteur
		FROM oeuvre
		WHERE noOeuvre = :id";
        $prep = $this->db->prepare($requete);
        $prep->bindParam(':id', $id, PDO::PARAM_INT);
        $prep->execute();
        $result = $prep->fetch();
        return $result;
    }

    function updateAndPersistOeuvre($id, $donnees){
        $date = DateTime::createFromFormat('d/m/Y', $donnees['dateParution']);
        $donnees['dateParution'] = $date->format('Y-m-d');
        $requete = "
		UPDATE oeuvre SET
		titre = '${donnees['titre']}'
		, dateParution = '${donnees['dateParution']}'
        , photo = '${donnees['photo']}'
		, idAuteur = '${donnees['idAuteur']}'
		WHERE noOeuvre = ${donnees['noOeuvre']};
		";
        $nbRes = $this->db->exec($requete);
        return $nbRes;
    }
}
