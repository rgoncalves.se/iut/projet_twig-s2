<?php

class EmpruntModel
{

	private $db;

	public function __construct()
	{
		$MaConnexion = new Connexion();
		$this->db = $MaConnexion->connect();
	}

	function createAndPersistEmprunt($donnees)
	{

	}


	function updateAndPersistEmprunt($donnees)
	{

	}

	// exemplaires des oeuvres disponibles

	function findExemplaireOeuvreDispo()
	{

	}


	function removeByIdEmprunt($donnees)
	{

	}


	function findAdherentEmprunt($idAdherent = "")
	{

	}


	function findByIdAdherentEmprunt($idAdherent)
	{
		$requete = "
    	SELECT emprunt.idAdherent
    	, emprunt.dateRendu 
    	FROM emprunt
    	INNER JOIN adherent ON adherent.idAdherent = emprunt.idAdherent
    	WHERE emprunt.idAdherent = " . $idAdherent . ";
    	";
		$select = $this->db->query($requete);
		$results = $select->fetchAll();
		return $results;
	}

	function findByIdExemplaireEmprunt($noExemplaire)
	{

	}

	// nombre d'exemplaires en retard pour un adhérent
	function findNbExempairesRetardAdherent($idAdherent)
	{

	}

	// exemplaires des oeuvres disponibles
	function findExemplairesArendre($idAdherent)
	{

	}


	////////// pour emprunter un livre


	// liste des adhérents ayant le droit d'emprunter un livre (pour la liste déroulante des adhérents lors d'un emprunt

	function findEmpruntDropdownAdherents()
	{

	}

	// liste des adhérents ayant le droit d'emprunter un livre (pour la liste déroulante des adhérents lors d'un emprunt

	function findEmpruntsByOneAdherent($idAdherent)
	{

	}

	// liste des adhérents ayant le droit d'emprunter un livre (pour la liste déroulante des adhérents lors d'un emprunt

	function findEmpruntReturnDropdownAdherents()
	{

	}

	// bilan

	function findEmpruntsBilan()
	{

	}

}

