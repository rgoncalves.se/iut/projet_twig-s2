<?php

class AdherentModel
{

	private $db;

	public function __construct() {
		$MaConnexion = new Connexion();
		$this->db = $MaConnexion->connect();
	}

	function findAllAdherents() {
		$requete = "
		SELECT ad.nomAdherent, ad.adresse, ad.datePaiement, ad.idAdherent
		, COUNT(empr.idAdherent) as nbrEmprunt
		, IF(CURRENT_DATE()>DATE_ADD(datePaiement, INTERVAL 1 YEAR),1,0) as retard
		, IF(CURRENT_DATE()>DATE_ADD(datePaiement, INTERVAL 11 MONTH),1,0) as retardProche
		, DATE_ADD(datePaiement, INTERVAL 1 YEAR) as datePaiementFutur
		FROM adherent ad
		LEFT JOIN emprunt empr ON empr.idAdherent=ad.idAdherent
		AND empr.dateRendu = '00-00-0000'
		GROUP BY ad.idAdherent 
		ORDER BY ad.nomAdherent;
		";
		$select = $this->db->query($requete);
		$results = $select->fetchAll();
		return $results;
	}

	function findAllDropdownAdherents() {

	}

	function createAndPersistAdherent($donnees) {
        $date = DateTime::createFromFormat('d/m/Y', $donnees['datePaiement']);
        $donnees['datePaiement'] = $date->format('Y-m-d');
		$requete = "
		INSERT INTO adherent(idAdherent, nomAdherent, adresse, datePaiement) VALUES
		(NULL, '${donnees['nomAdherent']}', '${donnees['adresse']}', '${donnees['datePaiement']}');
		";
		$nbRes = $this->db->exec($requete);
	}


	function removeByIdAdherent($id) {
		$requete = "
		DELETE FROM adherent
		WHERE idAdherent=" . $id . " LIMIT 1;
		";
		$nbRes = $this->db->exec($requete);
		return $nbRes;
	}

	function findOneByIdAdherent($id) {
		$requete = "
		SELECT idAdherent, nomAdherent, adresse, datePaiement
		FROM adherent
		WHERE idAdherent = :id";
		$prep = $this->db->prepare($requete);
		$prep->bindParam(':id', $id, PDO::PARAM_INT);
		$prep->execute();
		$result = $prep->fetch();
		return $result;
	}

	function updateAndPersistAdherent($id, $donnees) {
		$requete = "
		UPDATE adherent SET
		nomAdherent = '${donnees['nomAdherent']}'
		, adresse = '${donnees['adresse']}'
		, datePaiement = '${donnees['datePaiement']}'
		WHERE idAdherent = ${donnees['idAdherent']};
		";
		$nbRes = $this->db->exec($requete);
		return $nbRes;
	}


}

